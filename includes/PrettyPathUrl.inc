<?php

/**
 * @file
 * Provides a helper class for dealing with components of an URL.
 */

/**
 * Class PrettyPathUrl
 */
class PrettyPathUrl {

  /**
   * Current set of segments, representing the current path.
   *
   * @var array
   */
  protected $segments = array();

  /**
   * Current query params.
   *
   * @var array
   */
  protected $query = array();

  /**
   * Current fragment.
   *
   * @var string
   */
  protected $fragment = NULL;

  /**
   * The original path this object was initialized with.
   *
   * @var string
   */
  private $initial_path = '';

  /**
   * The original query parameters this object was initialized with.
   *
   * @var string
   */
  private $initial_query = array();

  /**
   * The original fragment this object was initialized with.
   *
   * @var string
   */
  private $initial_fragment = NULL;

  /**
   * Constructor.
   *
   * @param string $path
   * @param array $query
   * @param string $fragment
   */
  public function __construct($path, $query = array(), $fragment = NULL) {
    $this->segment_position = 0;
    $this->init($path, $query, $fragment);
  }

  /**
   * Initialize.
   *
   * @param string $path
   * @param array $query
   * @param string $fragment
   */
  protected function init($path, $query, $fragment) {
    $this->initial_path = $path;
    $this->setSegmentsFromPath($path);
    $this->query = $this->initial_query = $query;
    $this->fragment = $this->initial_fragment = $fragment;
  }

  /**
   * Helper to create the segments array from the given path.
   *
   * We never deal with the path directly, but extract it to a segments array.
   *
   * @param string $path
   */
  protected function setSegmentsFromPath($path) {
    $this->segments = explode('/', $path);
  }

  /**
   * Getter for the query params.
   *
   * @return array
   */
  public function getQuery() {
    return $this->query;
  }

  /**
   * Setter for whole query params array.
   *
   * @param $query
   *
   * @return PrettyPathUrl
   */
  public function setQuery($query) {
    $this->query = $query;
    return $this;
  }

  /**
   * Getter for the path.
   *
   * @return string
   */
  public function getPath() {
    return implode('/', $this->getSegments());
  }

  /**
   * Setter for the whole path.
   *
   * @param string $path
   *
   * @return PrettyPathUrl
   */
  public function setPath($path) {
    $this->setSegmentsFromPath($path);
    return $this;
  }


  /**
   * Getter for the segments array.
   *
   * @return array
   */
  public function getSegments() {
    return $this->segments;
  }

  /**
   * Getter for a single segment.
   *
   * @param integer $pos
   *   Position for the facet.
   *
   * @return string
   *   Returns the segment. If the segment is not valid, NULL is returned.
   */
  public function getSegment($pos) {
    if (isset($this->segments[$pos])) {
      return $this->segments[$pos];
    }
  }

  /**
   * Sets a path segment.
   *
   * @param integer $path
   * @param string $segment
   *
   * @return PrettyPathUrl
   */
  public function setSegment($pos, $segment) {
    // @todo: validate segment position.
    $this->segments[$pos] = $segment;
    ksort($this->segments);
    return $this;
  }

  /**
   * Removes a path segment from the given position.
   *
   * @param integer $pos
   */
  public function removeSegment($pos) {
    if (isset($this->segments[$pos])) {
      unset($this->segments[$pos]);
    }
  }

  /**
   * Get a PrettyPathUrl object with the initial values.
   *
   * @return PrettyPathUrl
   *   A new object holding the values, this object was intantiated with.
   */
  public function getInitialUrl() {
    return new PrettyPathUrl($this->initial_path, $this->initial_query, $this->initial_fragment);
  }

  /**
   * Checks if the url has changed since its initialisation.
   *
   * @return bool
   *   TRUE if path, query or fragment changed.
   */
  public function hasChanged() {
    // Check path.
    if ($this->initial_path != $this->getPath()) {
      return TRUE;
    }
    elseif (serialize($this->initial_query) != serialize($this->query)) {
      return TRUE;
    }
    elseif ($this->initial_fragment != $this->fragment) {
      return TRUE;
    }
    return FALSE;
  }

}
