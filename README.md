
## Purpose

This module provides a configuration for prettifying paths. Prettifying means to
replace query parameters with parts of the path. You e.g. may want to append the
page number to the path, isntead of carrying as query param.

Altering paths this way, may not have a big impact on SEO, so most times it may
not be worth the effort to deal with maybe occuring side effects. But if you
need exactly that behaviour, try this module and
[please give feedback](https://drupal.org/project/issues/2050427), so we can
know what pitfalls this approach comes with and maybe can provide some solution.

## Settings

@todo: A lot of things have changed since the last README update.
@see prettypath.api.php

## Known Issues

* When using with globalredirect module, you should apply the patch from
  [Do not use altered options for url_outbound alias #2060123](https://drupal.org/node/2060123).
