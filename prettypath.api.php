<?php

/**
 * @file
 * Holds hook definition for the API provided by prettypath.
 */

/**
 * Definition of multiple prettypath configurations.
 *
 * @return array
 *   Array of config definitions, each array consists of at least:
 *   - path: drupal_match()-style string to define for which paths this
 *     configuration should take effect
 *   - segments: array of segment configurations to bild a pretty path segment
 *     processor from
 *
 * @see PrettyPathSegmentProcessorBase
 */
function hook_prettypath_config() {
  $configs = array();
  // Provide a prettypath configuration that will convert all page params to
  // part of the url.
  $configs[] = array(
    'path' => '*',
    'segments' => array(
      array(
        'segment position' => 'by prefix',
        'type' => 'single',
        'prefix' => 'page-',
        'items' => array(
          array(
            'item' => array(
              'type' => 'get',
              'key' => 'page',
            ),
          ),
        ),
      ),
    ),
    'weight' => 100,
  );
  return $configs;
}
