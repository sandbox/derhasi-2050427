<?php

/**
 * @file
 * Provides interface for prettpath plugins.
 */

/**
 * Class PrettyPathPlugin.
 */
abstract class PrettyPathPlugin {

  /**
   * Holds the initial configuration for this plugin.
   *
   * @var array
   */
  protected $config;

  /**
   * Holds annotations that is only for external information and will not change
   * behaviour of the single plugin itself.
   *
   * @var array
   */
  public $annotations = array();

  /**
   * Constructor.
   *
   * @param array $config
   *   The plugin's instance configuration.
   * @param array $annotations
   *   Optional attributes for the plugin, but only for external use. These
   *   values shall not change the behaviour of the plugin itself.
   */
  public function __construct($config, $annotations = array()) {
    $this->setConfig($config);
    $this->setAnnotations($annotations);
    $this->init();
  }

  /**
   * Initialize configuration after construction.
   *
   * @param array $config
   *   Configuration array for the plugin instance.
   */
  protected function setConfig(array $config) {
    $this->config = $config;
  }

  /**
   * Set annotations construction.
   *
   * @param array $config
   *   Configuration array for the plugin instance.
   */
  public function setAnnotations(array $annotations) {
    $this->annotations = $annotations;
  }

  /**
   * Helper to check if the configuration is valid.
   *
   * @return bool
   *   Returns TRUE if the configuration is valid, e.g. no params are missing.
   *   FALSE otherwise.
   */
  public function configurationIsValid() {
    return TRUE;
  }

  /**
   * Initialize the plugin instance after default config is provided.
   */
  abstract protected function init();

}
