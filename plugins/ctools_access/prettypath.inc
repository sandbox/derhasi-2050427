<?php

/**
 * @file
 * Plugin to provide access control/visibility based prettypath URL.
 */

$plugin = array(
  'title' => t('Prettypath: current URL'),
  'description' => t('Control access by the current pretty or ugly url.'),
  'callback' => 'prettypath_ctools_access_prettypath_check',
  'settings form' => 'prettypath_ctools_access_prettypath_settings_form',
  'summary' => 'prettypath_ctools_access_prettypath_summary',
  'default' => array(
    'visibility_setting' => 'allow',
    'paths' => '',
    'parts' => array(),
  ),
);

/**
 * Settings form
 */
function prettypath_ctools_access_prettypath_settings_form($form, &$form_state, $conf) {

  $form['settings']['parts'] = array(
    '#type' => 'checkboxes',
    '#title' => t('URL parts'),
    '#description' => t('Select which parts of the URL shall be used for comparison'),
    '#options' => prettypath_ctools_access_prettypath_parts(),
    '#default_value' => $conf['parts'],
  );

  $form['settings']['visibility_setting'] = array(
    '#type' => 'radios',
    '#options' => array(
      'allow' => t('Allow access on the following pages'),
      'deny' => t('Allow access on all pages except the following pages'),
    ),
    '#default_value' => $conf['visibility_setting'],
  );

  $form['settings']['paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Paths'),
    '#default_value' => $conf['paths'],
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>')),
  );
  return $form;
}

/**
 * @return array
 *   Array of prettypath url parts
 */
function prettypath_ctools_access_prettypath_parts() {
  return array(
    'ugly_path' => t('Ugly path'),
    'ugly_query' => t('Ugly query params'),
    'pretty_path' => t('Pretty path'),
    'pretty_query' => t('Pretty query params'),
    'ugly_alias' => t('Alias of ugly path'),
  );
}

/**
 * Check for access.
 */
function prettypath_ctools_access_prettypath_check($conf, $context) {
  // Retrieves the current url as prettypath url object.
  $current_url = prettypath_current_url();

  $parts = array_filter($conf['parts']);
  if (empty($parts)) {
    $parts = prettypath_ctools_access_prettypath_parts();
  }

  $page_match = FALSE;

  // Check on any ugly part.
  if (!empty($parts['ugly_path']) || !empty($parts['ugly_query']) || !empty($parts['ugly_alias'])) {

    // We first will uglify our url.
    prettypath_uglify($current_url);

    // Check on the ugly path first.
    if (!empty($parts['ugly_path'])) {
      $page_match = drupal_match_path($current_url->getPath(), $conf['paths']);
    }
    // Check on ugly query if we did not have a match before.
    if (!$page_match && !empty($parts['ugly_query'])) {
      $query = drupal_http_build_query($current_url->getQuery());
      $page_match = drupal_match_path($query, $conf['paths']);
    }
    // Check on ugly path's alias.
    if (!$page_match && !empty($parts['ugly_alias'])) {
      $query = drupal_get_path_alias($current_url->getPath());
      $page_match = drupal_match_path($query, $conf['paths']);
    }
  }

  // If there is not match yet, we try the pretty path.
  if (!$page_match
    && (!empty($parts['pretty_path']) || !empty($parts['pretty_query']))) {

    // Convert to the pretty version.
    prettypath_prettify($current_url);

    // Check on the pretty path first.
    if (!empty($parts['pretty_path'])) {
      $page_match = drupal_match_path($current_url->getPath(), $conf['paths']);
    }
    // Check on pretty query if we did not have a match before.
    if (!$page_match && !empty($parts['ugly_query'])) {
      $query = drupal_http_build_query($current_url->getQuery());
      $page_match = drupal_match_path($query, $conf['paths']);
    }
  }

  // When visibility setting is to allow if page matches, we can simply pass it.
  // Otherwise we have to pass the inverse and deny access on a match.
  return ($conf['visibility_setting'] == 'allow') ? $page_match: !$page_match;
}

/**
 * Provide a summary description.
 */
function prettypath_ctools_access_prettypath_summary($conf, $context) {
  $paths = array();
  foreach (explode("\n", $conf['paths']) as $path) {
    $paths[] = check_plain($path);
  }

  $parts = prettypath_ctools_access_prettypath_parts();
  $conf['parts'] = array_filter($conf['parts']);
  if (!empty($conf['parts'])) {
    $parts = array_intersect_key($parts, $conf['parts']);
  }
  $identifier = t('Current URL (@parts)', array('@parts' => implode(', ', $parts)));

  if ($conf['visibility_setting']) {
    return format_plural(count($paths), '@identifier is "@paths"', '@identifier is one of "@paths"', array('@paths' => implode(', ', $paths), '@identifier' => $identifier));
  }
  else {
    return format_plural(count($paths), '@identifier is not "@paths"', '@identifier is not one of "@paths"', array('@paths' => implode(', ', $paths), '@identifier' => $identifier));
  }
}
