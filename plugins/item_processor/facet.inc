<?php

/**
 * @file
 * Holds pretty path item processor for retrieving a query parameter.
 */

/**
 * Class PrettyPathItemProcessorGet
 */
class PrettyPathItemProcessorFacet extends PrettyPathItemProcessorBase {

  /**
   * {@inheritdoc}
   */
  protected function setConfig(array $config) {
    parent::setConfig($config);

    // Ensure multiple value separator setting.
    if (!isset($this->config['separator'])) {
      $this->config['separator'] = '-';
    }

    // Default filter key is 'f'.
    if (!isset($this->config['filter key'])) {
      $this->config['filter key'] = 'f';
    }
  }


  /**
   * {@inheritdoc}
   */
  public function prettifyItem(PrettyPathUrl $url) {

    // We use the initial url to get our query from, as this may hold facets,
    // that have already been used by a segment processor or item before.
    $init_query = $url->getInitialUrl()->getQuery();
    $values = $this->getValuesFromFacets($init_query, $this->config['filter key'], $this->config['facet']);

    // To remove the facets from the url, we have to use the current url, not
    // the initial.
    $query = $url->getQuery();
    $this->removeFacet($query, $this->config['filter key'], $this->config['facet']);

    // Set the altered query params back to the url.
    $url->setQuery($query);

    $return = array();
    foreach ($values as $value) {
      $ret = $this->prettifyValue($value);
      // Valid values are not null, so we only add setted values to our list.
      if (isset($ret)) {
        $return[] = $ret;
      }
    }

    // Do return nothing, if there are no valid pretty values.
    if (!count($return)) {
      return;
    }

    // @todo escape separator
    return implode($this->config['separator'], $return);
  }

  /**
   * {@inheritdoc}
   */
  public function uglifyItem($segment_part, PrettyPathUrl $url) {
    $values = explode($this->config['separator'], $segment_part);

    $uglies = array();
    foreach ($values as $value) {
      $ugly_value = $this->uglifyValue($value);
      if (isset($ugly_value)) {
        $uglies[] = $ugly_value;
      }
    }

    // Proceed only, if we have something to set.
    if (count($uglies)) {

      // Finally set the uglified values back to the get param.
      $query = $url->getQuery();

      // Set the retrieved values back to the facet.
      $this->setValuesToFacets($query, $this->config['filter key'], $this->config['facet'], $uglies);

      $url->setQuery($query);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeItem(PrettyPathUrl $url) {

    // Remove the facet params for the configured facet.
    $query = $url->getQuery();
    $this->removeFacet($query, $this->config['filter key'], $this->config['facet']);
    // Set altered query back to the url.
    $url->setQuery($query);
  }


  /**
   * {@inheritdoc}
   */
  public function configurationIsValid() {
    return isset($this->config['facet']);
  }

  /**
   * Retrieve values for a given facet from an array.
   *
   * @param array $query
   *   Array of query parameters.
   * @param string $filter_key
   *   The key the facet filters are located in.
   * @param $name
   *  Name of the facet.
   *
   * @return array
   *   Array of values for the given facet.
   */
  protected function getValuesFromFacets($query, $filter_key, $name) {
    $return = array();

    if (isset($query[$filter_key])) {
      foreach ($query[$filter_key] as $key => $val) {
        if (strpos($val, $name . ':') === 0) {
          // Value is everthing behind the facet name.
          $return[] = drupal_substr($val, drupal_strlen($name . ':'));
        }
      }
    }
    return $return;
  }

  /**
   * Places the values to a facet in the facet params.
   *
   * @param array $query
   *   Array of query parameters by reference.
   * @param string $filter_key
   *   The key the facet filters are located in.
   * @param string $name
   *   The given facet's name.
   * @param array $values
   *   The values for the given facet.
   */
  protected function setValuesToFacets(&$query, $filter_key, $name, $values) {
    foreach ($values as $val) {
      $query[$filter_key][] = $name . ':' . $val;
    }
    $this->cleanFacets($query, $filter_key);
  }

  /**
   * Helps removing facets with the given name.
   *
   * @param array $query
   *   Array of query parameters by reference.
   * @param string $filter_key
   *   The key the facet filters are located in.
   * @param $name
   *   The name of the facet to remove.
   */
  protected function removeFacet(&$query, $filter_key, $name) {
    if (isset($query[$filter_key])) {
      foreach ($query[$filter_key] as $key => $val) {
        if (strpos($val, $name . ':') === 0) {
          // Extract the value from the facet string.
          unset($query[$filter_key][$key]);
        }
      }
      $this->cleanFacets($query, $filter_key);
    }
  }

  /**
   * Helper to clean a facets array.
   *
   * @param array $query
   *   Array of query parameters by reference.
   * @param string $filter_key
   *   The key the facet filters are located in.
   */
  protected function cleanFacets(&$query, $filter_key) {
    if (!isset($query[$filter_key])) {
      return;
    }

    // Clean up duplicates and empty ones.
    $query[$filter_key] = array_filter(array_unique($query[$filter_key]));

    if (count($query[$filter_key]) < 1) {
      unset($query[$filter_key]);
    }
  }


}
