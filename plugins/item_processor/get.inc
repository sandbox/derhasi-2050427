<?php

/**
 * @file
 * Holds pretty path item processor for retrieving a query parameter.
 */

/**
 * Class PrettyPathItemProcessorGet
 */
class PrettyPathItemProcessorGet extends PrettyPathItemProcessorBase {

  /**
   * {@inheritdoc}
   */
  protected function setConfig(array $config) {
    parent::setConfig($config);

    // Ensure multiple value separator setting.
    if (!isset($this->config['separator'])) {
      $this->config['separator'] = '-';
    }

    // Default filter key is 'f'.
    if (!isset($this->config['filter key'])) {
      $this->config['filter key'] = 'f';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prettifyItem(PrettyPathUrl $url) {
    $query = $url->getQuery();

    // If we got no get key, we cannot prettify.
    if (!isset($query[$this->config['key']])) {
      return;
    }

   // Get the values form our url and make sure we generally handle multiple values.
    $values = $query[$this->config['key']];
    if (!is_array($values)) {
      $values = array($values);
    }

    // We remove the get params from the url.
    unset($query[$this->config['key']]);
    $url->setQuery($query);

    $return = array();
    foreach ($values as $value) {
      $ret = $this->prettifyValue($value);
      // Valid values are not null, so we only add setted values to our list.
      if (isset($ret)) {
        $return[] = $ret;
      }
    }

    // Do return nothing, if we there are no valid pretty values.
    if (!count($return)) {
      return;
    }

    return implode($this->config['separator'], $return);
  }

  /**
   * {@inheritdoc}
   */
  public function uglifyItem($segment_part, PrettyPathUrl $url) {
    $values = explode($this->config['separator'], $segment_part);

    $uglies = array();
    foreach ($values as $value) {
      $ugly_value = $this->uglifyValue($value);
      if (isset($ugly_value)) {
        $uglies[] = $ugly_value;
      }
    }

    // Proceed only, if we have something to set.
    if (count($uglies)) {
      // Degrade single value to a string.
      if (count($uglies) == 1) {
        $uglies = reset($uglies);
      }

      // Finally set the uglified values back to the get param.
      $query = $url->getQuery();
      $query[$this->config['key']] = $uglies;
      $url->setQuery($query);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeItem(PrettyPathUrl $url) {

    // Remove the query param.
    $query = $url->getQuery();
    unset($query[$this->config['key']]);
    $url->setQuery($query);
  }


  /**
   * {@inheritdoc}
   */
  public function configurationIsValid() {
    return isset($this->config['key']) && isset($this->config['separator']);
  }

}
