<?php

/**
 * @file
 * Provides interface/abstract for prettypath item processor plugins.
 *
 * @see PrettyPathProcessor
 */

/**
 * Class PrettyPathItemProcessorBase.
 */
abstract class PrettyPathItemProcessorBase extends PrettyPathPlugin {

  /**
   * @var PrettyPathValueProcessorBase[]
   */
  protected $value_processors = array();

  /**
   * {@inheritdoc}
   */
  protected function init() {
    // Instantiate value processors.
    if (isset($this->config['processors'])) {
      foreach ($this->config['processors'] as $definition) {
        if (isset($definition['type'])) {
          $this->value_processors[] = prettypath_get_value_processor($definition['type'], $definition);
        }
      }
    }
  }

  /**
   * Helper to prettify given parts of an url and return a segment part.
   *
   * @param PrettyPathUrl $url
   *   The url to prettify. Be aware, that the url may already be processed by
   *   segment and items processors running before this one. To get the
   *   unprocessed url you should use the $url->getInitialUrl().
   *   The url may be altered by the method. But altering the segments should be
   *   avoided, as the segment processors take care about that.
   *
   * @return string
   *   The pretty part of the path to put in the segment. If no item could be
   *   created NULL shall be returned.
   */
  abstract public function prettifyItem(PrettyPathUrl $url);

  /**
   * Helper to uglify a given segment part.
   *
   * @param string $segment_part
   *   If there is a segment part defined in the pretty path, the part related to
   *   that single item is passed.
   * @param PrettyPathUrl $url
   *   The url to uglify.
   */
  abstract public function uglifyItem($segment_part, PrettyPathUrl $url);

  /**
   * Helper to remove parts of the url, that are part of the item.
   *
   * @param PrettyPathUrl $url
   *   Url to remove the item from.
   */
  abstract public function removeItem(PrettyPathUrl $url);

  /**
   *
   * @param mixed $value
   */
  public function prettifyValue($value) {
    $return = $value;
    foreach ($this->value_processors as $processor) {
      $return = $processor->prettify($return);
    }
    return $return;
  }

  /**
   *
   * @param mixed $value
   */
  public function uglifyValue($value) {
    $return = $value;
    foreach ($this->value_processors as $processor) {
      $return = $processor->uglify($return);
    }
    return $return;
  }



}
