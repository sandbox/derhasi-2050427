<?php

/**
 * @file
 * Segment processor for a single item.
 */

/**
 * Class PrettyPathSegmentProcessorMutiple
 */
class PrettyPathSegmentProcessorSingle extends PrettyPathSegmentProcessorBase {

  /**
   * Holds the single item.
   *
   * @var PrettyPathItemProcessorBase
   */
  protected $item;

  /**
   * {@inheritdoc}
   */
  protected function init() {
    parent::init();

    if (isset($this->items[0])) {
      $this->item = &$this->items[0];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prettifySegment(PrettyPathUrl $url) {
    $position = $this->getPosition($url);

    // As this is the "single" plugin, we only handle a single item.
    $segment = $this->item->prettifyItem($url);

    // If we got segment parts, we can join them to tge the new segment.
    if (isset($segment)) {

      // Add configured prefix and suffix.
      $segment = $this->addFixes($segment);

      // Provide altered values back to the url.
      $url->setSegment($position, $segment);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function uglifySegment(PrettyPathUrl $url) {
    $position = $this->getPosition($url);

    // We do not use the initial URL to support 'auto' segment positions.
    // @see PrettyPathSegmentProcessorMultiple::uglifySegment()
    $segment = $url->getSegment($position);

    // If the segment is not present, we do not need to process.
    if (!isset($segment)) {
      return FALSE;
    }

    // Before processing the value, we have to remove prefix and suffix.
    $segment = $this->removeFixes($segment);

    // As this is the "single" plugin, we only handle a single item.
    $this->item->uglifyItem($segment, $url);

    // In the end, we simply remove the segment.
    $url->removeSegment($position);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function removeSegment(PrettyPathUrl $url) {
    // Remove the pretty part.
    $position = $this->getPosition($url);
    $url->removeSegment($position);
    $this->item->removeItem($url);
  }

  /**
   * {@inheritdoc}
   */
  public function configurationIsValid() {
    // Fail if parent config is invalid.
    if (!parent::configurationIsValid()) {
      return FALSE;
    }
    // We always need a valid item.
    return isset($this->item);
  }

}
