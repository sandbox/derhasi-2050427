<?php

/**
 * @file
 * Segment processor for multiple items.
 */

/**
 * Class PrettyPathSegmentProcessorMutiple
 */
class PrettyPathSegmentProcessorMultiple extends PrettyPathSegmentProcessorBase {

  /**
   * {@inheritdoc}
   */
  protected function setConfig(array $config) {
    parent::setConfig($config);
    // Ensure multiple items separator setting.
    if (!isset($this->config['separator'])) {
      $this->config['separator'] = '_';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prettifySegment(PrettyPathUrl $url) {
    $position = $this->getPosition($url);

    // We collect all segment parts from the defined items.
    $parts = array();
    foreach ($this->items as $item) {
      $new_segment_part = $item->prettifyItem($url);
      if (isset($new_segment_part)) {
        $parts[] = array(
          'part' => $new_segment_part,
          'item' => $item,
        );
      }
    }

    // The collected items will be processed by our submethod to build the new
    // segment string.
    $new_segment = $this->implodeSegments($parts);

    // If we got segment parts, we can join them to tge the new segment.
    if (isset($new_segment)) {

      // Add configured prefix and suffix.
      $new_segment = $this->addFixes($new_segment);

      // Provide altered values back to the url.
      $url->setSegment($position, $new_segment);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function uglifySegment(PrettyPathUrl $url) {
    $position = $this->getPosition($url);

    // We do not use the initial URL, as there should be only one segment,
    // altering a segment. This way, we also can support 'auto' segment
    // positions. As those cleanup the segment, before the original segment is
    // processed.
    $segment = $url->getSegment($position);

    // If the segment is not present, we do not need to process.
    if (!isset($segment)) {
      return FALSE;
    }

    // We remove that segment, as it is not intended to be there after uglifying
    // this segment.
    $url->removeSegment($position);

    // Before processing the value, we have to remove prefix and suffix.
    $segment = $this->removeFixes($segment);

    // For each part, we process the associated item.
    // @todo: unescape separator
    $parts = $this->explodeSegments($segment);
    foreach ($parts as $part) {
      $item = $part['item'];
      $segment_part = $part['part'];
      $item->uglifyItem($segment_part, $url);
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function removeSegment(PrettyPathUrl $url) {
    // Remove the pretty part.
    $position = $this->getPosition($url);
    $url->removeSegment($position);
    // Let the items remove their (ugly) parts.
    foreach ($this->items as $item) {
      $item->removeItem($url);
    }
  }

  /**
   * Implodes parts of a segment to one big string.
   *
   * @param array $parts
   *   Array of part definitions, each holding:
   *   - 'part': part of the segment
   *   - 'item': associated item processor
   *
   * @return string
   *   Concecanated string for the segment (without prefix and suffix).
   *
   * @see PrettyPathSegmentProcessorMultiple::explodeSegments()
   */
  protected function implodeSegments($parts) {
    $new_segment_parts = array();

    foreach ($parts as $part) {
      $item = $part['item'];
      $new_segment_part = $part['part'];
      // @todo: escape prefixes
      $new_segment_parts[] = $item->annotations['prefix'] . $new_segment_part;
    }

    if (count($new_segment_parts)) {
      // @todo: escape separator
      return implode($this->config['separator'], $new_segment_parts);
    }
  }

  /**
   * Explodes segment string to segment parts with items associated.
   *
   * @param string $segment
   *   The segment string (wihtout prefix or suffix) to split up.
   *
   * @return array
   *   array of arrays holding 'part' and 'item':
   *   - 'part': part of the segment
   *   - 'item': associated item processor
   *
   * @see PrettyPathSegmentProcessorMultiple::implodeSegments()
   */
  protected function explodeSegments($segment) {

    $return = array();

    // For each part, we collect the associated item.
    // @todo: unescape separator
    $parts = explode($this->config['separator'], $segment);
    foreach ($parts as $part) {
      // We extract the prefix, so we can associate an item processor to the
      // given part.
      $match = array();
      // Only proceed if we find a prefix.
      if (preg_match('/^([A-z]{1,2})(.+)$/', $part, $match)) {
        $prefix = $match[1];
        $value = $match[2];

        foreach ($this->items as $item) {
          if ($item->annotations['prefix'] == $prefix) {
            $return[] = array(
              'part' => $value,
              'item' => $item,
            );
            break;
          }
        }
      }
    }
    return $return;
  }
}
