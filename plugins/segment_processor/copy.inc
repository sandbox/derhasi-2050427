<?php

/**
 * @file
 * Segment processor for copying multiple items as representatives of an
 * existing one.
 */

/**
 * Class PrettyPathSegmentProcessorCopy
 */
class PrettyPathSegmentProcessorCopy extends PrettyPathSegmentProcessorBase {

  /**
   * {@inheritdoc}
   */
  protected function setConfig(array $config) {
    parent::setConfig($config);
    // Ensure multiple items separator setting.
    if (!isset($this->config['separator'])) {
      $this->config['separator'] = '_';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prettifySegment(PrettyPathUrl $url) {
    $position = $this->getPosition($url);

    // We collect all segment parts form the defined items and simply append
    // them one to another. We do not need an item specific prefix, as this
    // segment processor does not uglify.
    $new_segment_parts = array();
    foreach ($this->items as $item) {
      $new_segment_part = $item->prettifyItem($url);
      if (isset($new_segment_part)) {
        $new_segment_parts[] = $new_segment_part;
      }
    }

    // If we got segment parts, we can join them to get the new segment.
    if (count($new_segment_parts)) {
      // @todo: escape separator
      $new_segment = implode($this->config['separator'], $new_segment_parts);

      // Add configured prefix and suffix.
      $new_segment = $this->addFixes($new_segment);

      // Provide altered values back to the url.
      $url->setSegment($position, $new_segment);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function uglifySegment(PrettyPathUrl $url) {
    $position = $this->getPosition($url);

    // We do nothing to recreate ugly values. We only make sure, we removed the
    // pretty segment from the path.
    $url->removeSegment($position);

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function removeSegment(PrettyPathUrl $url) {
    // Remove the pretty part.
    $position = $this->getPosition($url);
    $url->removeSegment($position);
    // Let the items remove their (ugly) parts.
    foreach ($this->items as $item) {
      $item->removeItem($url);
    }
  }

}
