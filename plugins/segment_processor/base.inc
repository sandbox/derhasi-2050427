<?php

/**
 * @file
 * Holds the interface for pretty path segment processors.
 */

/**
 * Class PrettyPathSegmentProcessorBase
 *
 * Example config:
 * @code
 * $config = array(
 *   'type' => 'multiple',
 *   // Set this to 'auto' if you want an automatic mapping on prefix/suffix.
 *   // Either prefix or suffix or both have to be presetn for that.
 *   // In 'auto' mode, the segment is automaticall appended as last segment, if
 *   // no existing segment is found.
 *   'segment position' => 1,
 *   // Set 'required' if this segment shall be required for the following
 *   // segments.
 *   'required' => 1,
 *   // Set 'force' if this segment shall be processed, even if a earlier one
 *   // was required.
 *   'force' => 1,
 *   // The placeholder for an item not beeing required.
 *   'placeholder' => '-',
 *   'separator' => '-',
 *   'prefix' => '',
 *   'suffix' => '',
 *   'items' => array(
 *     array(
 *       'prefix' => 't',
 *       'item' => array(
 *         'type' => 'get',
 *         'key' => 'title',
 *         'value processors' => array(),
 *       ),
 *     ),
 *     array(
 *       'prefix' => 'p',
 *       'item' => array(
 *         'type' => 'get',
 *         'key' => 'page',
 *         'value processors' => array(),
 *       ),
 *     ),
 *   ),
 * );
 * @endcode
 * Definition of item's value processors are documented in the item processor.
 *
 * @see PrettyPathItemProcessorBase
 */
abstract class PrettyPathSegmentProcessorBase extends PrettyPathPlugin {

  /**
   * Holds the item defintions.
   *
   * @var PrettyPathItemProcessorBase[]
   */
  protected $items = array();

  /**
   * {@inheritdoc}
   */
  protected function setConfig(array $config) {
    $this->config = $config;

    // Default prefix and suffix are simply empty strings.
    if (!isset($this->config['prefix'])) {
      $this->config['prefix'] = '';
    }
    if (!isset($this->config['suffix'])) {
      $this->config['suffix'] = '';
    }

    // Set the default placeholder configuration.
    if (!isset($this->config['placeholder'])) {
      $this->config['placeholder'] = '-';
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function init() {
    $this->items = array();
    foreach ($this->config['items'] as $item_definition) {
      // We got no item processor, if there is no type defined.
      if (isset($item_definition['item']['type'])) {
        $item_processor = prettypath_get_item_processor($item_definition['item']['type'], $item_definition['item']);
        // Remove the item definition, the rest is part of the anntotation.
        unset($item_definition['item']);
        $item_processor->setAnnotations($item_definition);
        $this->items[] = $item_processor;
      }
    }
  }

  /**
   * Prettify the given segment.
   *
   * @param PrettyPathUrl $url
   *   The url object for the current url to alter.
   *
   * @return bool
   *   Returns FALSE if the segment could not be processed in the way it had to.
   */
  abstract public function prettifySegment(PrettyPathUrl $url);

  /**
   * Uglify the given segment.
   *
   * @param PrettyPathUrl $url
   *   The url object for the current url to alter.
   *
   * @return bool
   *  Returns FALSE if the segment could not be processed in the way it had to.
   */
  abstract public function uglifySegment(PrettyPathUrl $url);

  /**
   * Check if this segment is needed to build the next segment.
   *
   * When this returns true, the path will be cleaned up by all ugly parts.
   *
   * @return bool
   */
  public function isRequired() {
    return !empty($this->config['required']);
  }

  /**
   * Check if the segment shall be processed anyway.
   *
   * @return bool
   *   TRUE if the segment shall be processed in any case.
   */
  public function isForced() {
    return !empty($this->config['force']);
  }

  /**
   * Set a placeholder for this segment, when no pretty segment could be set.
   *
   * @param PrettyPathUrl $url
   *   The url object for the current url to alter.
   */
  public function setPrettyPlaceholder(PrettyPathUrl $url) {
    $position = $this->getPosition($url);
    // We only set this, if the segment is not already set.
    $current = $url->getSegment($position);
    if (!isset($current)) {
      $url->setSegment($position, $this->config['placeholder']);
    }
  }

  /**
   * Check if the segement processor shall init placement of placeholders.
   */
  public function initPrettyPlaceholderPlacementOnSuccess() {
    // By default, placeholders are not necessary, if we got an auto-positioned
    // element.
    return ($this->config['segment position'] != 'auto');
  }

  /**
   * Remove any components ugly and pretty for the given component.
   *
   * This method is called, when child entries shall be removed from the Url.
   *
   * @param PrettyPathUrl $url
   *   The url object for the current url to alter.
   */
  abstract public function removeSegment(PrettyPathUrl $url);

  /**
   * Helper to check if the configuration is valid.
   *
   * @return bool
   *   Returns TRUE if the configuration is valid, e.g. no params are missing.
   *   FALSE otherwise.
   */
  public function configurationIsValid() {
    // If we are in segment position auto mode, we need prefix or suffix.
    if ($this->config['segment position'] == 'auto'
      && drupal_strlen($this->config['prefix'] . $this->config['suffix']) == 0) {
      return FALSE;
    }

    return isset($this->config['items']) && is_array($this->config['items']);
  }

  /**
   * Helper for this plugin to get only the position.
   *
   * @param PrettyPathUrl $url
   *   The url object for the current url to alter.
   *
   * @return int
   *   The segment position to alter.
   */
  public function getPosition($url) {
    // We got an auto mapping for the position by prefix/suffix.
    if ($this->config['segment position'] == 'auto') {
      $segments = $url->getSegments();
      foreach ($segments as $position => $segment) {
        // If we get a value from the segment, this is the position to use.
        $success = $this->removeFixes($segment, TRUE);
        if (isset($success)) {
          return $position;
        }
      }
      // If we did not find anything, the last segment is the place to go for.
      return $position + 1;
    }

    return $this->config['segment position'];
  }

  /**
   * Adds prefix and suffix to the segment.
   *
   * @param string $segment
   *   Segment without prefix and suffix.
   *
   * @return string
   *   Segment with prefix and suffix.
   */
  protected function addFixes($segment) {
    // Do not add prefix and suffix if we got an empty string.
    if (!drupal_strlen($segment)) {
      return '';
    }

    return $this->config['prefix'] . $segment . $this->config['suffix'];
  }

  /**
   * Removes prefix and suffix from a segment string.
   *
   * @param string $segment
   *   The segment string to extract from
   * @param bool $strict
   *   Only returns a value, if configured prefix and suffix are given.
   *
   * @return string
   *   Segment without prefix or suffix.
   *
   */
  protected function removeFixes($segment, $strict = TRUE) {

    // Remove prefix ...
    $len = drupal_strlen($this->config['prefix']);
    if ($len > 0) {
      if (strpos($segment, $this->config['prefix']) === 0) {
        $segment = drupal_substr($segment, $len);
      }
      // In strict mode we do not return anything, as the prefix was required.
      elseif ($strict) {
        return;
      }
    }
    // ... and suffix.
    $len = drupal_strlen($this->config['suffix']);
    if ($len > 0) {
      $pos = drupal_strlen($segment) - $len;
      if (prettypath_strpos($segment, $this->config['suffix']) === $pos) {
        $segment = drupal_substr($segment, 0, $pos);
      }
      // In strict mode we do not return anything, as the prefix was required.
      elseif ($strict) {
        return;
      }
    }

    return $segment;
  }

}
