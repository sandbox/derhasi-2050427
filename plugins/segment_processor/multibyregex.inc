<?php

/**
 * @file
 * Segment processor for multiple items, that does not use a specific separator
 * to distinguish items, but uses a regex to extract prefix and value.
 *
 * Example config:
 * @code
 * $config = array(
 *   'type' => 'multiregex',
 *   'regex pattern prefix' => '([a-z]{1,2})',
 *   'regex delimiter' => '/',
 *   'items' => array(
 *     array(
 *       'prefix' => 't',
 *       'item' => array( … ),
 *     ),
 *     array(
 *       'prefix' => 'p',
 *       'item' => array( … ),
 *     ),
 *   ),
 * );
 * @endcode
 *
 */

/**
 * Class PrettyPathSegmentProcessorMultibyregex
 */
class PrettyPathSegmentProcessorMultibyregex extends PrettyPathSegmentProcessorMultiple {

  /**
   * Holds the processed regex for extracting the prefix with preg_split.
   *
   * @var string
   */
  protected $regex_prefix = '';

  /**
   * {@inheritdoc}
   */
  protected function setConfig(array $config) {
    parent::setConfig($config);
    // This processor does not work with a separator.
    unset($this->config['separator']);

    // The regex delimiter defaults to '/', leave empty if the regex shall
    if (!isset($this->config['regex delimiter'])) {
      $this->config['regex delimiter'] = '/';
    }

  }

  /**
   * {@inheritdoc}
   */
  protected function init() {
    parent::init();

    // Build the regex pattern from the regex pattern prefix config option.
    $regex_prefix = $this->config['regex pattern prefix'];

    // If there is no bracket marker for a result, we automatically add it.
    // [a-z] converts to ([a-z]), so preg_split can list the delimiters in the
    // result.
    // We only can process that, if delimiters haven't been added.
    if (drupal_strlen($this->config['regex delimiter']) > 0 && strpos($regex_prefix, '(') === FALSE) {
      $regex_prefix = "($regex_prefix)";
    }

    // Add delimiters.
    if (drupal_strlen($this->config['regex delimiter']) > 0) {
      $regex_prefix = $this->config['regex delimiter'] . $regex_prefix . $this->config['regex delimiter'];
    }


    $this->regex_prefix = $regex_prefix;
  }

  /**
   * {@inheritdoc}
   */
  public function configurationIsValid() {
    // Fail if parent config is invalid.
    if (!parent::configurationIsValid()) {
      return FALSE;
    }
    // We allways need a regex pattern for the prefix.
    return isset($this->config['regex pattern prefix']);
  }

  /**
   * {@inheritdoc}
   */
  protected function implodeSegments($parts) {
    $new_segment_parts = array();

    foreach ($parts as $part) {
      $item = $part['item'];
      $new_segment_part = $part['part'];
      // @todo: escape prefixes
      $new_segment_parts[] = $item->annotations['prefix'] . $new_segment_part;
    }

    if (count($new_segment_parts)) {
      return implode('', $new_segment_parts);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function explodeSegments($segment) {

    $return = array();

    $split = preg_split($this->regex_prefix, $segment, 0, PREG_SPLIT_DELIM_CAPTURE);

    //  If we got split segments, we found some values.
    if (count($split)) {

      // The first element is without prefix, so we skip it.
      array_shift($split);
      // loop through all parts, as long we find a prefix.
      while ($prefix = array_shift($split)) {
        // The element after a prefix is the segment part.
        $part = array_shift($split);

        // Now look for the associated item and populate that relation to our
        // return value.
        foreach ($this->items as $item) {
          if ($item->annotations['prefix'] == $prefix) {
            $return[] = array(
              'part' => $part,
              'item' => $item,
            );
            break;
          }
        }
      }
    }

    return $return;
  }
}
