<?php

/**
 * @file
 * Base class for converting ugly to pretty paths and vice versa.
 */

/**
 * Class PrettyPathProcessor.
 */
class PrettyPathProcessor extends PrettyPathPlugin {

  /**
   * Segment processors for the given pretty path configuration.
   *
   * @var PrettyPathSegmentProcessorBase[]
   */
  protected $segments = array();

  /**
   * {@inheritdoc}
   */
  protected function init() {
    // Instantiate all segment processors.
    foreach ($this->config['segments']  as $position => $conf) {
      // We use the segment processor of the specified type to get the ugly
      // version.
      $this->segments[$position] = prettypath_get_segment_processor($conf['type'], $conf);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function configurationIsValid() {
    return TRUE;
  }

  /**
   * Reconverts the the URI to the query based facets, so uglifies it.
   *
   * @param PrettyPathUrl
   *   Object representing the url to process.
   *
   * @see PrettyPathProcessor::prettify()
   */
  public function uglify($url) {

    // Extract the components from the given path. We process in reverse order,
    // so those segements that added last will remove their segment first.
    // This way it is for example possible to better support 'auto' segment
    // positions.
    foreach (array_reverse($this->segments) as $segment_processor) {

      // $query is altered by the uglifySegment method.
      $segment_processor->uglifySegment($url);
    }
  }

  /**
   * Prettifies the given URI.
   *
   * @param PrettyPathUrl $url
   *   Object representing the url to process.
   *
   * @see PrettyPathProcessor::uglify()
   */
  function prettify($url) {

    // First make sure we got the ugliest version possible.
    $this->uglify($url);

    // We set the ugly version as the initial one ot work with, so the
    // prettifier has all ugly components available. This way no segment
    // processor will fail, if it looks on the inital path, that should be the
    // ugliest one.
    $ugly_url = prettypath_url($url->getPath(), $url->getQuery());

    $erase = FALSE;
    $placeholding_queue = array();

    // Process every segment with its segment processor.
    foreach ($this->segments as $segment_processor) {

      // Try to set a pretty segment, as long we are not in erase mode or the
      // segment processor forces to do so.
      if (empty($erase) || $segment_processor->isForced()) {
        // Prettify the segment.
        $success = $segment_processor->prettifySegment($ugly_url);

        // We stop collecting parts, as we will not have a valid path, when we
        // continue.
        if ($success === FALSE) {

          // So we switch in erase mode for the next segments if those require
          // the current one.
          if ($segment_processor->isRequired()) {
            $erase = TRUE;
          }
          // Otherwise we collect placeholders, we will use to fill the path if
          // we find a successful segment.
          else {
            $placeholding_queue[] = $segment_processor;
          }
        }
        // If prettifying was successfull, we maybe have to add the placeholders
        // for he former unsuccessfull elements to the path, so it will a valid
        // url without segment gaps.
        // We only process this, if the current segment processor does not veto.
        // This is the case, when there is 'auto' segment position enabled.
        elseif (!empty($placeholding_queue) && $segment_processor->initPrettyPlaceholderPlacementOnSuccess()) {
          foreach ($placeholding_queue as $processor) {
            $processor->setPrettyPlaceholder($ugly_url);
          }
          // Reset placeholder queue.
          $placeholding_queue = array();
        }
      }
      // If we are in erase mode and the segment is not forced, we simply remove
      // the segment from the path.
      else {
        $segment_processor->removeSegment($ugly_url);
      }
    }

    // At the end, we set back the result to our incoming url.
    $url->setPath($ugly_url->getPath());
    $url->setQuery($ugly_url->getQuery());
  }

  /**
   * Decide if the processor instance shall be used to process the url.
   *
   * @param PrettyPathUrl $url
   *   The base url this processor should work on.
   *
   * @return bool
   *   Returns TRUE if the instance shall process the given URL.
   */
  public function isActive($url) {
    return drupal_match_path($url->getPath(), $this->config['path']);
  }

}
