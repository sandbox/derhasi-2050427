<?php

/**
 * @file
 * Holds the interface for pretty path value processors.
 */

/**
 * Class PrettyPathValueProcessorBase
 */
abstract class PrettyPathValueProcessorBase extends PrettyPathPlugin {

  /**
   * {@inheritdoc}
   */
  protected function init() {
  }

  /**
   * Converts the ugly value to a prettier one to use in the path.
   *
   * @param mixed $value
   *   The representation of the value of the ugly path.
   *
   * @return string
   *   The represantation of the value to show in the pretty path.
   */
  abstract public function prettify($value);

  /**
   * Converts the pretty value back, to the one to be used in the ulgy path.
   *
   * @param string $value
   *   Value from the pretty path.
   *
   * @return $mixed
   *   Value to pass to the ugly path.
   */
  abstract public function uglify($value);

}
