<?php

/**
 * @file
 * Holds a value processor to map specific values to a prettier string.
 *
 * @code
 * $config = array(
 *   'type' => 'mapping',
 *   'mapping' => array(
 *     '1' => 'low',
 *     '2' => 'medium',
 *     '3' => 'high',
 *   ),
 * );
 * @endcode
 *
 */

/**
 * Class PrettyPathValueProcessorMapping.
 */
class PrettyPathValueProcessorMapping extends PrettyPathValueProcessorBase {

  /**
   * Helper for easy access to the mapping.
   *
   * @var array
   */
  protected $mapping = array();

  /**
   * Helper for easy access to mapping a value to a key.
   *
   * @var array
   */
  protected $reverse_mapping = array();

  /**
   * {@inheritdoc}
   */
  protected function setConfig(array $config) {
    parent::setConfig($config);

    // Ensure mapping is set.
    if (!isset($this->config['mapping'])) {
      $this->config['mapping'] = array();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function init() {
    // Populate helper variables.
    $this->mapping = $this->config['mapping'];
    $this->reverse_mapping = array_flip($this->mapping);
  }


  /**
   * {@inheritdoc}
   */
  public function prettify($value) {
    if (isset($this->mapping[$value])) {
      return (string) $this->mapping[$value];
    }
    return (string) $value;
  }

  /**
   * {@inheritdoc}
   */
  public function uglify($value) {
    if (isset($this->reverse_mapping[$value])) {
      return (string) $this->reverse_mapping[$value];
    }
    return (string) $value;
  }

}
