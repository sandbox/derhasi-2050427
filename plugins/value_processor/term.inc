<?php

/**
 * @file
 * Converts a term id to its term name.
 *
 * @code
 * $config = array(
 *   'type' => 'term',
 *   'vocabularies' => array(
 *     'tags',
 *   ),
 * );
 * @endcode
 */

/**
 * Class PrettyPathValueProcessorTerm.
 */
class PrettyPathValueProcessorTerm extends PrettyPathValueProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function prettify($value) {

    // The value has to be a term id.
    if (is_numeric($value)) {
      $term = taxonomy_term_load($value);
      // Proceed if the term exists and is in one of the specified vocabularies.
      if ($term && (empty($this->config['vocabularies']) || in_array($term->vocabulary_machine_name, $this->config['vocabularies']))) {
        return strtolower($term->name);
      }
    }

    return;
  }

  /**
   * {@inheritdoc}
   */
  public function uglify($value) {
    // @todo: ensure strtolowered term names are retrieved too.
    if (empty($this->config['vocabularies'])) {
      $terms = taxonomy_get_term_by_name($value);
    }
    else {
      foreach ($this->config['vocabularies'] as $voc) {
        $terms = taxonomy_get_term_by_name($value, $voc);
        if (!empty($terms)) {
          break;
        }
      }
    }
    // Only return a value, if we got at least one valid term.
    if (!empty($terms)) {
      return $terms[0]->tid;
    }
  }

}
