<?php

/**
 * @file
 * Holds the raw value processor.
 */

/**
 * Class PrettyPathValueProcessorRaw.
 */
class PrettyPathValueProcessorRaw extends PrettyPathValueProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function prettify($value) {
    return (string) $value;
  }

  /**
   * {@inheritdoc}
   */
  public function uglify($value) {
    return $value;
  }

}
